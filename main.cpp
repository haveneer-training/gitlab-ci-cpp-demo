#include <iostream>

int main() {
#ifndef NDEBUG
    std::cout << "[Running in Debug mode]\n";
#else
    std::cout << "[Running in Release mode]\n";
#endif
    std::cout << "Hello, World!" << std::endl;
      return 0; // bad format
}
