[![pipeline status](https://gitlab.com/haveneer-training/gitlab-ci-cpp-demo/badges/master/pipeline.svg)](https://gitlab.com/haveneer-training/gitlab-ci-cpp-demo/-/commits/master)
[![coverage report](https://gitlab.com/haveneer-training/gitlab-ci-cpp-demo/badges/master/coverage.svg)](https://gitlab.com/haveneer-training/gitlab-ci-cpp-demo/-/commits/master)

# Cpp Ci Demo

Gitlab Pages are available at [https://haveneer-training.gitlab.io/gitlab-ci-cpp-demo](https://haveneer-training.gitlab.io/gitlab-ci-cpp-demo). 

Pour optimiser l'installation des packages:
* voir [gitlab-ci-demo/cache-for-installation-speedup](https://gitlab.com/haveneer-training/gitlab-ci-demo/-/blob/cache-for-installation-speedup/.gitlab-ci.yml)